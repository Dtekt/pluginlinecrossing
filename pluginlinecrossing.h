#ifndef PLUGINLINECROSSING_H
#define PLUGINLINECROSSING_H

#include <QObject>
#include "plugininterface.h"

class PluginLineCrossing : public QObject, public PluginInterface
{
    Q_OBJECT
    Q_INTERFACES(PluginInterface)
public:
    explicit PluginLineCrossing();
    void prepareGUI(MainWindow * win) const;
    void detectAndDisplay(MainWindow *win, FrameInfo* frameInfo) const;
};

#endif // PLUGINLINECROSSING_H

#include "pluginlinecrossing.h"
#include "ui_mainwindow.h"
#include <QQueue>
#include <QDateTime>

PluginLineCrossing::PluginLineCrossing()
{
    pluginName = "Line Crossing Detection";
    pluginDescription = "This plugin detects line crossings in a scene";
}

void PluginLineCrossing::prepareGUI(MainWindow * win) const
{
    win->ui->menuPlugins->addAction(pluginName);
    win->ui->logView->appendPlainText(pluginName + " plugin loaded..\n" + QDateTime::currentDateTime().toString() + "\n");
}

void PluginLineCrossing::detectAndDisplay(MainWindow *win, FrameInfo *frameInfo) const
{
//    TODO: Add code here
}

Q_EXPORT_PLUGIN2(pluginlinecrossing,PluginLineCrossing)

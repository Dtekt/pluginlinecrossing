QT       += gui

TEMPLATE = lib
CONFIG  += plugin
DEFINES += PLUGINLINECROSSING_LIBRARY
SOURCES += pluginlinecrossing.cpp
HEADERS += pluginlinecrossing.h \
    plugininterface.h \
    mainwindow.h \
    ui_mainwindow.h \
    frameinfo.h
TARGET   = $$qtLibraryTarget(pluginlinecrossing)
DESTDIR  = ../PluginLineCrossing/plugins
INCLUDEPATH += C:\C\opencv2.3.1\build\include
CONFIG(release,debug|release)
{
    LIBS += C:\C\OpenCV2.3.1\build\gpu\x86\lib\opencv_calib3d231.lib \
            C:\C\OpenCV2.3.1\build\gpu\x86\lib\opencv_contrib231.lib \
            C:\C\OpenCV2.3.1\build\gpu\x86\lib\opencv_core231.lib \
            C:\C\OpenCV2.3.1\build\gpu\x86\lib\opencv_features2d231.lib \
            C:\C\OpenCV2.3.1\build\gpu\x86\lib\opencv_flann231.lib \
            C:\C\OpenCV2.3.1\build\gpu\x86\lib\opencv_gpu231.lib \
            C:\C\OpenCV2.3.1\build\gpu\x86\lib\opencv_haartraining_engine.lib \
            C:\C\OpenCV2.3.1\build\gpu\x86\lib\opencv_highgui231.lib \
            C:\C\OpenCV2.3.1\build\gpu\x86\lib\opencv_imgproc231.lib \
            C:\C\OpenCV2.3.1\build\gpu\x86\lib\opencv_legacy231.lib \
            C:\C\OpenCV2.3.1\build\gpu\x86\lib\opencv_ml231.lib \
            C:\C\OpenCV2.3.1\build\gpu\x86\lib\opencv_objdetect231.lib \
            C:\C\OpenCV2.3.1\build\gpu\x86\lib\opencv_ts231.lib \
            C:\C\OpenCV2.3.1\build\gpu\x86\lib\opencv_video231.lib
}
CONFIG(debug,debug|release)
{
    LIBS += C:\C\OpenCV2.3.1\build\gpu\x86\lib\opencv_calib3d231d.lib \
            C:\C\OpenCV2.3.1\build\gpu\x86\lib\opencv_contrib231d.lib \
            C:\C\OpenCV2.3.1\build\gpu\x86\lib\opencv_core231d.lib \
            C:\C\OpenCV2.3.1\build\gpu\x86\lib\opencv_features2d231d.lib \
            C:\C\OpenCV2.3.1\build\gpu\x86\lib\opencv_flann231d.lib \
            C:\C\OpenCV2.3.1\build\gpu\x86\lib\opencv_gpu231d.lib \
            C:\C\OpenCV2.3.1\build\gpu\x86\lib\opencv_haartraining_engined.lib \
            C:\C\OpenCV2.3.1\build\gpu\x86\lib\opencv_highgui231d.lib \
            C:\C\OpenCV2.3.1\build\gpu\x86\lib\opencv_imgproc231d.lib \
            C:\C\OpenCV2.3.1\build\gpu\x86\lib\opencv_legacy231d.lib \
            C:\C\OpenCV2.3.1\build\gpu\x86\lib\opencv_ml231d.lib \
            C:\C\OpenCV2.3.1\build\gpu\x86\lib\opencv_objdetect231d.lib \
            C:\C\OpenCV2.3.1\build\gpu\x86\lib\opencv_ts231d.lib \
            C:\C\OpenCV2.3.1\build\gpu\x86\lib\opencv_video231d.lib
}
